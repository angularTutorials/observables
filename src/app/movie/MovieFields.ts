export enum MovieFields {
  movie_id,
  title,
  phase,
  category_name,
  release_year,
  running_time,
  rating_name,
  disc_format_name,
  number_discs,
  viewing_format_name,
  aspect_ratio_name,
  status,
  release_date,
  budget,
  gross,
  time_stamp
}
