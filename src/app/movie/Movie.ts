export class Movie {

  public constructor(
    private _movie_id: number,
    private _title: string,
    private _phase: string,
    private _category_name: string,
    private _release_year: number,
    private _running_time: number,
    private _rating_name: string,
    private _disc_format_name: string,
    private _number_discs: number,
    private _viewing_format_name: string,
    private _aspect_ratio_name: string,
    private _status: string,
    private _release_date: string,
    private _budget: number,
    private _gross: number,
    private _time_stamp: Date) {
  }

  public toString = (): string => {

    return `Movie (movie_id: ${this._movie_id},
    title: ${this._title},
    phase: ${this._phase},
    category_name: ${this._category_name},
    release_year: ${this._release_year},
    running_time: ${this._running_time},
    rating_name: ${this._rating_name},
    disc_format_name: ${this._disc_format_name},
    number_discs: ${this._number_discs},
    viewing_format_name: ${this._viewing_format_name},
    aspect_ratio_name: ${this._aspect_ratio_name},
    status: ${this._status},
    release_date: ${this._release_date},
    budget: ${this._budget},
    gross: ${this._gross},
    time_stamp: ${this._time_stamp})`;

  };

  get movie_id(): number {
    return this._movie_id;
  }

  set movie_id(value: number) {
    this._movie_id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get phase(): string {
    return this._phase;
  }

  set phase(value: string) {
    this._phase = value;
  }

  get category_name(): string {
    return this._category_name;
  }

  set category_name(value: string) {
    this._category_name = value;
  }

  get release_year(): number {
    return this._release_year;
  }

  set release_year(value: number) {
    this._release_year = value;
  }

  get running_time(): number {
    return this._running_time;
  }

  set running_time(value: number) {
    this._running_time = value;
  }

  get rating_name(): string {
    return this._rating_name;
  }

  set rating_name(value: string) {
    this._rating_name = value;
  }

  get disc_format_name(): string {
    return this._disc_format_name;
  }

  set disc_format_name(value: string) {
    this._disc_format_name = value;
  }

  get number_discs(): number {
    return this._number_discs;
  }

  set number_discs(value: number) {
    this._number_discs = value;
  }

  get viewing_format_name(): string {
    return this._viewing_format_name;
  }

  set viewing_format_name(value: string) {
    this._viewing_format_name = value;
  }

  get aspect_ratio_name(): string {
    return this._aspect_ratio_name;
  }

  set aspect_ratio_name(value: string) {
    this._aspect_ratio_name = value;
  }

  get status(): string {
    return this._status;
  }

  set status(value: string) {
    this._status = value;
  }

  get release_date(): string {
    return this._release_date;
  }

  set release_date(value: string) {
    this._release_date = value;
  }

  get budget(): number {
    return this._budget;
  }

  set budget(value: number) {
    this._budget = value;
  }

  get gross(): number {
    return this._gross;
  }

  set gross(value: number) {
    this._gross = value;
  }

  get time_stamp(): Date {
    return this._time_stamp;
  }

  set time_stamp(value: Date) {
    this._time_stamp = value;
  }
}
