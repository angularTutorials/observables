import {Injectable} from '@angular/core';
import {Movie} from './movie/Movie';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';

@Injectable()
export class ImdbApiServiceService {

  private moviesJson = 'assets/movies.json';

  constructor(private http: HttpClient) {
  }

  public fetchOneById(id: number): Observable<Movie> {
    let resultMovie: Movie;

    return this.http.get<Movie>(this.moviesJson)
      .filter((a: any) => {
        return a.movies.filter(movie => {
          return movie.movie_id === +id;
        }).map((movie: any) => {
          resultMovie = movie;
        });
      })
      .map(() => {
        return resultMovie;
      });
  }

/*  search(term: string): Observable<Movie[]> {
    return this.http.get(this.moviesJson)
      .map(res => {
        return res.json().results.map(item => {
          return new Movie();
        });
      });
  }*/
}
