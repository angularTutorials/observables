import {Component} from '@angular/core';
import {Movie} from './movie/Movie';
import {ImdbApiServiceService} from './imdb-api-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'this works fine!';
  public movies: Movie[];
  public movieById: Movie;
  public movieTitle: string;
  public movieId: number;
  constructor(private imdbApiService: ImdbApiServiceService) {
    this.movieId = 1;
    imdbApiService.fetchOneById(this.movieId).subscribe(value => {
      this.movieById = value;
      this.movieTitle = value.title;
    });
  }
  public showMovieById(movieId: number): void {
    this.imdbApiService.fetchOneById(movieId).subscribe(value => {
      this.movieById = value;
      this.movieTitle = value.title;
    });
  }
}
