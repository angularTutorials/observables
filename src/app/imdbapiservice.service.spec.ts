import { TestBed, inject } from '@angular/core/testing';

import { ImdbApiServiceService } from './imdb-api-service.service';

describe('ImdbApiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImdbApiServiceService]
    });
  });

  it('should be created', inject([ImdbApiServiceService], (service: ImdbApiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
